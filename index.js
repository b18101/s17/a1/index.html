/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	let fullName = prompt("What is your name?");
	
	function yourName(){
		console.log("Hello," + " " + fullName);
	};

	yourName();


	let age = prompt("How old are you?");

	function yourAge(){
		console.log("You are" + " " + age + " " + "years old");
	};
	
	yourAge();

	let address = prompt("Where do you live?");

	function yourAddress(){
		console.log("You live in" + " " + address);
	}
	
	yourAddress();

	alert("Thanks for your input!");

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favBans(){
		let band1 = "1. Kamikazee";
		let band2 = "2. Ben&Ben"
		let band3 = "3. Parokya ni Edgar"
		let band4 = "4. Linkin Park"
		let band5 = "5. Simple Plan"

		console.log(band1);
		console.log(band2);
		console.log(band3);
		console.log(band4);
		console.log(band5);

	};

	favBans();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favMovies(){
		let movie1 = "1. Harry Potter and the Sorcerer's Stone";
		let rate1 = "81%"
		let movie2 = "2. Harry Potter and the Chamber of Secrets"
		let rate2 = "82%"
		let movie3 = "3. Harry Potter and the Prisoner of Azkaban"
		let rate3 = "90%"
		let movie4 = "4. Harry Potter and the Goblet of Fire"
		let rate4 = "88%"
		let movie5 = "5. Harry Potter and the Order of the Phoenix"
		let rate5 = "77%"

		console.log(movie1);
		console.log("Rotten Tomatoes Rating:" + " " + rate1);
		console.log(movie2);
		console.log("Rotten Tomatoes Rating:" + " " + rate2);
		console.log(movie3);
		console.log("Rotten Tomatoes Rating:" + " " + rate3);
		console.log(movie4);
		console.log("Rotten Tomatoes Rating:" + " " + rate4);
		console.log(movie5);
		console.log("Rotten Tomatoes Rating:" + " " +rate5);
	};

	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



let printUsers = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	// console.log(friends); 
};

printUsers();

// console.log(friend1);
// console.log(friend2);